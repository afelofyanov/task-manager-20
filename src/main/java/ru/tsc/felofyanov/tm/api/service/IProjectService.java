package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnerService<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    Project create(String userId, String name, String description, Date dateBegin, Date dateEnd);

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project changeProjectStatusById(String userId, String id, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);
}
