package ru.tsc.felofyanov.tm.api.repository;

import ru.tsc.felofyanov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getCommands();

    void add(AbstractCommand command);

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String argument);
}
