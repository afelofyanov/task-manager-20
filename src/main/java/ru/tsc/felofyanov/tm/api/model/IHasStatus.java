package ru.tsc.felofyanov.tm.api.model;

import ru.tsc.felofyanov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);
}
