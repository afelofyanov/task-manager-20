package ru.tsc.felofyanov.tm.api.repository;

import ru.tsc.felofyanov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);
}
