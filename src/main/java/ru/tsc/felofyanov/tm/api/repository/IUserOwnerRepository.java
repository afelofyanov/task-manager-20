package ru.tsc.felofyanov.tm.api.repository;

import ru.tsc.felofyanov.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerRepository<M extends AbstractUserOwnerModel> extends IRepository<M> {

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator comparator);

    M remove(String userId, M model);

    void clear(String userId);

    M add(String userId, M model);

    boolean existsById(String userId, String id);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);
}
