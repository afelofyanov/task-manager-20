package ru.tsc.felofyanov.tm.service;

import ru.tsc.felofyanov.tm.api.repository.ICommandRepository;
import ru.tsc.felofyanov.tm.api.service.ICommandService;
import ru.tsc.felofyanov.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(final AbstractCommand command) {
        commandRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (name == null && name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        if (argument == null && argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }
}
