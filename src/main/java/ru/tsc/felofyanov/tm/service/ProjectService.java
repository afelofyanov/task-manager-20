package ru.tsc.felofyanov.tm.service;

import ru.tsc.felofyanov.tm.api.repository.IProjectRepository;
import ru.tsc.felofyanov.tm.api.service.IProjectService;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.*;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.Date;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    public Project create(final String userId, final String name, final String description, final Date dateBegin, final Date dateEnd) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final Project project = create(userId, name, description);
        if (project == null) throw new ProjectNotFoundException();
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId, String id, Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setUserId(userId);
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, Integer index, Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IdEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setUserId(userId);
        project.setStatus(status);
        return project;
    }
}
