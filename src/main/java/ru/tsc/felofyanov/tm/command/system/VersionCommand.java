package ru.tsc.felofyanov.tm.command.system;

public final class VersionCommand extends AbstractSystemCommand {
    @Override
    public String getName() {
        return "version";
    }

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Show applicant version.";
    }

    @Override
    public void execute() {
        System.out.println("1.20.0");
    }
}
