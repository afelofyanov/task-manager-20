package ru.tsc.felofyanov.tm.command.project;

import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {
    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Project project = getServiceLocator().getProjectService().findOneById(userId, id);
        showProject(project);
    }
}
