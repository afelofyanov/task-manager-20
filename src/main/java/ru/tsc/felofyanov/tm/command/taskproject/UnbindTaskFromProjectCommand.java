package ru.tsc.felofyanov.tm.command.taskproject;

import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class UnbindTaskFromProjectCommand extends AbstractTaskProjectCommand {
    @Override
    public String getName() {
        return "unbind-task-to-project";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Unbind task from project.";
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");

        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();

        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        final String userId = getUserId();
        getServiceLocator().getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
    }
}
