package ru.tsc.felofyanov.tm.command.task;

import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {
    @Override
    public String getName() {
        return "task-start-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Start task by index.";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");

        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getServiceLocator().getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
    }
}
