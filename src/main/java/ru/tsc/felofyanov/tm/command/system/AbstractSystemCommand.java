package ru.tsc.felofyanov.tm.command.system;

import ru.tsc.felofyanov.tm.api.service.ICommandService;
import ru.tsc.felofyanov.tm.command.AbstractCommand;
import ru.tsc.felofyanov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {
    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }
}
