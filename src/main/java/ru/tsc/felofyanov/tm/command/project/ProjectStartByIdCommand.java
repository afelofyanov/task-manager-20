package ru.tsc.felofyanov.tm.command.project;

import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {
    @Override
    public String getName() {
        return "project-start-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Start project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getServiceLocator().getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
    }
}
