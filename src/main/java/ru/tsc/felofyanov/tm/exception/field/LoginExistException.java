package ru.tsc.felofyanov.tm.exception.field;

public final class LoginExistException extends AbstractFieldException {
    public LoginExistException() {
        super("Error! Login already exists...");
    }
}
